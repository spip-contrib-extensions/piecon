<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/piecon?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_piecon' => 'Configuración de Piecon',

	// E
	'explication_fallback' => 'El fallback es el método utilizado para cambiar tanto el title como el favicon de la página. Puede tener 3 valores:<br />
"false", el valor por defecto, no cambiará el favicon más que cuando sea posible<br />
"true", sólo el title será modificado indicando un porcentaje incluso si el navegador acepta el cambio de favicon<br />
"force", cambiará el favicon cuando sea posible así como el title de la página;',

	// L
	'label_background' => 'Color de fondo',
	'label_color' => 'Color',
	'label_fallback' => 'Método por defecto',
	'label_shadow' => 'Color de sombra',

	// O
	'option_fallback_false' => 'false',
	'option_fallback_force' => 'force',
	'option_fallback_true' => 'true'
);
