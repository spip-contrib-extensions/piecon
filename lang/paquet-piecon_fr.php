<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/piecon.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'piecon_description' => 'Ce plugin ne fait qu’insérer et configurer la librairie Javascript [Piecon->http://lipka.github.com/piecon/] dans SPIP',
	'piecon_nom' => 'Piecon',
	'piecon_slogan' => 'Insertion de la librairie Piecon dans SPIP'
);
