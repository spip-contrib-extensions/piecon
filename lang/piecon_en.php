<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/piecon?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_piecon' => 'Piecon setup',

	// E
	'explication_fallback' => 'The fallback method is used to change both the title or the favicon of the page. It can have three values​​: <br />
"false", the default, only change the favicon when possible;<br />
"true", only the title will be changed by specifying a percentage even if the browser accepts change of favicon;<br />
"force", change the favicon when possible as well as the title of the page;',

	// L
	'label_background' => 'Background color',
	'label_color' => 'Color',
	'label_fallback' => 'Default method',
	'label_shadow' => 'Shadow color',

	// O
	'option_fallback_false' => 'false',
	'option_fallback_force' => 'force',
	'option_fallback_true' => 'true'
);
