<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-piecon?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'piecon_description' => 'Este plugin simplemente inserta y configura la biblioteca Javascript [Piecon->http://lipka.github.com/piecon/] en SPIP',
	'piecon_nom' => 'Piecon',
	'piecon_slogan' => 'Inserción de la biblioteca Piecon en SPIP'
);
